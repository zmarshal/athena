/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/


/**
@page TrigVKalFitter_page TrigVKalFitter Package

Collection of trigger related interfaces to VKalVrt core. 

@author Vadim.Kostyukhin@cern.ch

@section TrigVKalFitter_TrigVKalFitterIntro Introduction

This package contains  interfaces to VKalVrt. 

@section TrigVKalFitter_TrigVKalFitterOverview Class Overview

  
 Some description of algorithms may be found in http://kostyuk.home.cern.ch/kostyuk/vertex/ and
 references inside.
 Software related issues may be viewed in
 https://twiki.cern.ch/twiki/bin/view/Atlas/VKalVrt .
  


*/
