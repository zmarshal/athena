#include "../DumpDecisions.h"
#include "../RoRSeqFilter.h"
#include "../TriggerSummaryAlg.h"
#include "../ComboHypo.h"
#include "../InputMakerForRoI.h"

DECLARE_COMPONENT( DumpDecisions )
DECLARE_COMPONENT( RoRSeqFilter )
DECLARE_COMPONENT( TriggerSummaryAlg )
DECLARE_COMPONENT( ComboHypo )
DECLARE_COMPONENT( InputMakerForRoI )


